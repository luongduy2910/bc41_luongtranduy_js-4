/**
 * ba ô input tương ứng với ba giá trị mà người dùng nhập 
 * 
 * xử lý :
 * DOM tới 3 ô input của người dùng để lấy giá trị và ép kiểu sang number bằng cách *1 
 * sử dụng if else lồng nhau để tìm số lớn nhất và số bé nhất
 * In ra kết quả 
 */


document.getElementById('tangdan').addEventListener("click", sapXepTangDan) ; 
function sapXepTangDan() {
    var num1 = document.getElementById('so1').value * 1 ; 
    var num2 = document.getElementById('so2').value * 1 ; 
    var num3 = document.getElementById('so3').value * 1 ; 

    if (num1 == num2 || num2 == num3 || num1 == num3 || num1 == num2 == num3 ) {
        document.getElementById('show-ketqua').innerHTML = `Xin vui lòng nhập 3 số khác nhau` ; 
    }
    
    else if (num1 > num2 && num1 >num3 ) { 
        if (num2 > num3 ) {
            document.getElementById('show-ketqua').innerHTML = `thứ tự tăng dần : ${num3} ${num2} ${num1}` ;
        }
        else {
            document.getElementById('show-ketqua').innerHTML = `thứ tự tăng dần : ${num2} ${num3} ${num1}` ;
        }
    }
    
    else if (num2 > num1 && num2 >num3 ) { 
        if (num1 > num3 ) {
            document.getElementById('show-ketqua').innerHTML = `thứ tự tăng dần : ${num3} ${num1} ${num2}` ; 
        }
        else {
            document.getElementById('show-ketqua').innerHTML = `thứ tự tăng dần : ${num1} ${num3} ${num2}` ; 
        }
    } 
    
    else if (num3 > num2 && num3 >num1 ) { 
        if (num2 > num1 ) {
            document.getElementById('show-ketqua').innerHTML = `thứ tự tăng dần : ${num1} ${num2} ${num3}` ; 
        }
        else {
            document.getElementById('show-ketqua').innerHTML = `thứ tự tăng dần : ${num2} ${num1} ${num3}` 
        }
    }
    else {
        document.getElementById('show-ketqua').innerHTML = `Vui lòng nhập 3 số` ;  
    } 
}




