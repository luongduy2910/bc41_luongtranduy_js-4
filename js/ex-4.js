/**
 * đầu vào là 3 cạnh của tam giác mà người dùng nhập vào 
 * 
 * xử lý :
 * DOM tới 3 ô input gán vào biến và ép kiểu sang number bằng cách * 1 
 * sử dụng if .... else if ..... để xử lý bài toán và if lồng vào nhau
 * kết hợp với toán tử && và || để thu hẹp điều kiện 
 */

document.getElementById('doantamgiac').addEventListener('click', doanTamGiac) ; 

function doanTamGiac() {
    var canh1 = document.getElementById('canh1').value * 1 ; 
    var canh2 = document.getElementById('canh2').value * 1 ; 
    var canh3 = document.getElementById('canh3').value * 1 ; 
    
    if (canh1 + canh2 > canh3 && canh2 + canh3 > canh1 && canh1 + canh3 > canh2) {
        if (canh1 === canh2 && canh1 !== canh3 || canh2 ===canh3 && canh3 !== canh1 || canh1 ===canh3 && canh1 !== canh2) {
           document.getElementById('show-ketqua3').innerHTML =`Đây là tam giác cân` 
        } 
        
        else if (canh1 === canh2 && canh1 === canh3) {
           document.getElementById('show-ketqua3').innerHTML =`Đây là tam giác đều` 
        }
        else if (canh1*canh1 == canh2*canh2 + canh3*canh3 || canh2*canh2 == canh1*canh1 + canh3*canh3 || canh3*canh3 == canh2*canh2 + canh1*canh1 ) {
            document.getElementById('show-ketqua3').innerHTML = `Đây là tam giác vuông`
        } 
        
        else {
            document.getElementById('show-ketqua3').innerHTML =`Đây là tam giác thường`

        }
        
    }
    else {
        document.getElementById('show-ketqua3').innerHTML =`Đây không phải là tam giác`

    }

}



